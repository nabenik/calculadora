package com.nabenik.demo2.calculadora;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author tuxtor
 */
public class CalculadoraTest {
    
    Calculadora calc;            
    
    @Before
    public void setUp() {
        calc = new Calculadora();
    }
    
    @Test
    public void testSum(){
        Assert.assertEquals(10, calc.sumar(5, 5));
    }
    
}
